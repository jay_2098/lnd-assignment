// Create a class for a book that represents book’s
//  information such as title, author, and publication year.
// The class should provide methods for retrieving and
// updating the book’s details. Additionally, implement a
// separate class for library that manages a collection of
// books, can be an array of books as well. The library
// class should have methods for adding books, searching
// for books by title or author, and displaying the total
// number of books in the library.

class Book {
  String title;
  String author;
  int publicationYear;

  Book({
    required this.title,
    required this.author,
    required this.publicationYear,
  });
}

class Library {
  List<Book> books = [];

  void addBook(Book book) {
    books.add(book);
  }

  void searchBookByTitle(String title) {
    var abc = books.where((book) => book.title == title);
    print(abc);
  }

  void searchBookByAuthor(String author) {
    var abc = books.where((book) => book.author == author);
    print(abc);
  }

  int getTotalCountofBooks() {
    return books.length;
  }
}

void main() {
  Book book1 = Book(
    title: 'Clean Code',
    author: 'Robert C Martin',
    publicationYear: 2012,
  );

  Book book2 = Book(
    title: 'An Autobiography',
    author: 'Jawaharlal Nehru',
    publicationYear: 2008,
  );

  Book book3 = Book(
    title: 'Anandmath',
    author: 'Bankim Chandra Chatterjee',
    publicationYear: 2010,
  );

  Library library = Library();

  library.addBook(book1);
  library.addBook(book2);
  library.addBook(book3);

  print(library.getTotalCountofBooks());
}
